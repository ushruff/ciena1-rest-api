import pandas as pd
import numpy as np
import mlflow

def predict_from_file(model_name, tag, file_path):
    mlflow.set_tracking_uri("sqlite:///mlflow.db")
    model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{tag}")

    data_x = pd.read_parquet(file_path, engine='pyarrow')
    preds = {"preds": (model.predict(data_x)).tolist()}
    preds_df = pd.DataFrame(preds['preds'], columns = ['pred'])
    preds_df.to_parquet('predictions/preds.parquet', engine='pyarrow')
    return preds
