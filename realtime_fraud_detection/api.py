import os
import flask
from flask import request, jsonify
import subprocess
import json

app = flask.Flask(__name__)
app.config["DEBUG"] = True

# class FaustApp:
#     data_in = faust_app.topic('data_in')
#     predictions = faust_app.topic("predictions")
#     def __init__(self):
#
#
#         mlflow.set_tracking_uri("sqlite:///mlflow.db")
#
#         self.model_name = "sklearn-log-regression"
#         self.stage = "production"
#         self.model = mlflow.pyfunc.load_model(model_uri=f"models:/{self.model_name}/{self.stage}")
#
#     @faust_app.agent(data_in, sink=[predictions])
#     async def process(self, data):
#         async for x in data:
#             y = self.model.predict(pd.DataFrame(np.array(x['data']), columns=x['columns']))
#             result = int(y)
#             print(f"Model Prediction: {result}")
#             yield (result)


@app.route('/', methods=['GET'])
def home():
    return '''<h1>Hello!</h1><p>This is a flask practice app.</p>'''


@app.route('/api/start', methods=['POST'])
def api_start():
    params = request.get_json()
    os.environ['CONFIG_NAME'] = params['config_name']
    # if query_parameters['source'] == "file":
    #     preds = from_file.predict_from_file(query_parameters['model'], query_parameters['tag'], query_parameters['file_path'])
    #     return jsonify(preds)

    subprocess.Popen(f'faust -A fraud_detection_worker worker -l info', shell=True)

    return jsonify(params)

@app.route('/api/stop', methods=['POST'])
def api_stop():
    # faust_worker = str(request.args['faust_worker'])
    #
    subprocess.run(f'pkill -9 -f fraud_detection_worker', shell=True)

    return '''<h1>Stopped Faust worker successfully!</h1>'''


@app.route('/api/new-config', methods=['POST'])
def api_new_config():
    params = request.get_json()
    config_name = params['config_name']
    params.pop('config_name')

    with open(f'configs/{config_name}.json', 'w') as outfile:
        json.dump(params, outfile)

    return params

@app.route('/api/get-configs', methods=['GET'])
def api_get_configs():
    configs = []
    directory = './configs'

    for entry in os.scandir(directory):
        if entry.is_file() and entry.name.endswith('.json'):
            with open(entry.path) as json_file:
                data = json.load(json_file)
                data['config_file_name'] = entry.name
                configs.append(data)

    return jsonify(configs)

@app.route('/api/delete-config', methods=['DELETE'])
def api_delete_config():
    config_name = request.get_json()['config_name']

    if (config_name.lower() == "all"):
        directory = './configs'
        for entry in os.scandir(directory):
            if entry.is_file() and entry.name.endswith('.json'):
                os.remove(entry.path)
    else:
        file_path = (f'./configs/{config_name}.json')
        if os.path.exists(file_path):
            os.remove(file_path)

    return api_get_configs()

# @app.route('/api/modify-config', methods=['PUT'])
# def api_get_configs():
#     config_name = request.get_json()['config_name']
#     with open(f'configs/{config_name}.json') as json_file:
#         config = json.load(json_file)

if __name__ == "__main__":
    app.run()