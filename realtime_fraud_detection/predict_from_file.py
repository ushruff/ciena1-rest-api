import pandas as pd
import numpy as np
import mlflow

def predic_from_file(model, stage):
    mlflow.set_tracking_uri("sqlite:///mlflow.db")
    model_name = "sklearn-log-regression"
    stage = "production"

    model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{stage}")

