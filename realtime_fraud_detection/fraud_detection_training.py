import pandas as pd
import numpy as np
import mlflow
import mlflow.sklearn
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, recall_score, precision_score

import logging

logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)


def eval_metrics(actual, pred):
    accuracy = accuracy_score(test_y, pred_y)
    f1 = f1_score(test_y, pred_y)
    precision =  precision_score(test_y, pred_y)
    recall = recall_score(test_y, pred_y)

    return accuracy, f1, precision, recall

np.random.seed(2021)
df = pd.read_csv('creditcard.csv')

# counting the values for eaach class
target_names = { 0: 'Not Fraud', 1: 'Fraud'}

# getting the columns for x and y
x_name = df.iloc[:, 1:30].columns
y_name = df.iloc[:1, 30: ].columns

data_x = df[x_name]
data_y = df[y_name]
train_x, test_x, train_y, test_y = train_test_split(data_x, data_y, train_size=0.7, test_size=0.3)

mlflow.set_tracking_uri("sqlite:///mlflow.db")

with mlflow.start_run():
    
    model = LogisticRegression(max_iter=2000)
    model.fit(train_x, train_y.values.ravel())

    pred_y = model.predict(test_x)

    accuracy, f1, precision, recall = eval_metrics(test_y, pred_y)
    mlflow.log_param("accuracy", accuracy)
    mlflow.log_param("f1", f1)
    mlflow.log_param("precision", precision)
    mlflow.log_param("recall", recall)
    
    # Log the sklearn model and register as version 1
    mlflow.sklearn.log_model(
        sk_model=model,
        artifact_path="model",
        registered_model_name="sklearn-log-regression"
    )

