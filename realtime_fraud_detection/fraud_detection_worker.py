import os
import faust
import pandas as pd
import mlflow
import json

app = faust.App(
    'fraud_detection',
    broker='kafka://kafka_broker:9092',
    value_serializer='json',
)

data_in = app.topic('data_in')
predictions = app.topic("predictions")

mlflow.set_tracking_uri("sqlite:///mlflow.db")

# model_name = "sklearn-log-regression"
# stage = "production"

with open('./configs/file_config.json') as json_file:
#with open(f'./configs/{os.getenv("CONFIG_NAME")}.json') as json_file:
    params = json.load(json_file)

model_name = params['model_name']
stage = params['model_tag']
model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{stage}")
data_dict = {}
if (params['input_source'] == 'file'):
    file_path = params['file_path']
    df = pd.read_parquet(file_path, engine='pyarrow')
    data_dict = df.to_dict(orient='records')

preds_list = []

@app.agent()
async def agent_b(data):
    async for x in data:
        y = model.predict(pd.DataFrame.from_records([x]))
        result = int(y)
        preds_list.append(result)
        print(f"Model Prediction: {result}")
        yield (result)
    preds_df = pd.DataFrame(preds_list, columns=['pred'])
    preds_df.to_parquet('./predictions/preds_new.parquet', engine='pyarrow')

@app.agent()
async def test(reply):
    async for reply in agent_b.map(data_dict):
        print(reply)

# @app.agent(data_in, sink=[predictions])
# async def process(data):
#     async for x in data:
#         y = model.predict(pd.DataFrame(np.array(x['data']), columns=x['columns']))
#         result = int(y)
#         print(f"Model Prediction: {result}")
#         yield (result)
#
# @app.agent(predictions)
# async def print_freq(preds):
#     async for pred in preds:
#         print(f"This was outputted to kafka: {pred}")

if __name__ == '__main__':
    app.main()
