import faust
import pandas as pd
import numpy as np
import mlflow


app = faust.App(
    'fraud_detection',
    broker='kafka://localhost:29092',
    value_serializer='json',
)

data_in = app.topic('data_in')
predictions = app.topic("predictions")

mlflow.set_tracking_uri("sqlite:///mlflow.db")

model_name = "sklearn-log-regression"
stage = "production"
model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{stage}")

@app.agent(data_in, sink=[predictions])
async def process(data):
    async for x in data:
        y = model.predict(pd.DataFrame(np.array(x['data']), columns=x['columns']))
        result = int(y)
        print(f"Model Prediction: {result}")
        yield (result)

@app.agent(predictions)
async def print_freq(preds):
    async for pred in preds:
        print(f"This was outputted to kafka: {pred}")

# if __name__ == '__main__':
#     app.main()
