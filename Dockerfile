FROM ubuntu:20.04

ENV FLASK_ENV=development
ENV FLASK_APP=api

RUN apt-get update -y && \
    apt-get install -y python3-pip python-dev

COPY ./realtime_fraud_detection ./flask_app

#RUN sudo apt update  pip install --upgrade pip

WORKDIR ./flask_app

RUN pip install -r requirements.txt

EXPOSE 8000

CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
#CMD faust -A fraud_detection_worker worker -l info